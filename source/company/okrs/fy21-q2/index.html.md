---
layout: markdown_page
title: "FY21-Q2 OKRs"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from May 1, 2020 to July 31, 2020.

## On this page
{:.no_toc} 

- TOC
{:toc}

## OKR Schedule
The by-the-book schedule for the OKR timeline would be 

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2020-03-30 | CEO pushes top goals to this page |
| -4 | 2020-04-06 | E-group pushes updates to this page and discusses it in the e-group weekly meeting |
| -3 | 2020-04-13 | E-group 50 minute draft review meeting |
| -2 | 2020-04-20 | Discuss with the teams |
| -1 | 2020-04-27 | CEO reports give a How to achieve presentation |
| 0  | 2020-05-04 | CoS updates OKR page for current quarter to be active |
| +2 | 2020-05-17 | Review previous and next quarter during the next board meeting |

## OKRs

### 1. CEO: IACV 
[Epic 408](https://gitlab.com/groups/gitlab-com/-/epics/408)

1. Increase [IACV](/handbook/sales/#incremental-annual-contract-value-iacv) per sales and marketing dollar spend. [Sales efficiency](/handbook/sales/#sales-efficiency-ratio) > 1.0.
1. More efficient marketing through reusablility. MRs per person, site/docs visits, ands Youtube upload goals?
1. Drive [IACV](/handbook/sales/#incremental-annual-contract-value-iacv) with growth initiatives. $xm generated.

### 2. CEO: Popular next generation product
[Epic 410](https://gitlab.com/groups/gitlab-com/-/epics/410)

1. Using an extra stage triples conversion. Increase [SpU](/handbook/product/metrics/#stages-per-user-spu) by 0.5 stages.
1. Deliver more value to users efficiently. Release posts items per engineer +25%
1. Reduce the cost of GitLab.com. to xxx [per user](/handbook/engineering/infrastructure/performance-indicators/#infrastructure-hosting-cost-per-gitlab-com-monthly-active-users)

### 3. CEO: Great team
[Epic 411](https://gitlab.com/groups/gitlab-com/-/epics/411)

1. On average team-members have 3 [Competencies](/handbook/competencies/) certifications.
1. More diverse company. Top of funnel is twice as diverse as the current [identity data](/company/culture/inclusion/identity-data/).
1. Metric driven. All KPIs in operational in Sisense, GitLab, or the handbook. Key meeting presentations have auto-updating graphs through the [automated KPI Slides](/handbook/finance/key-meetings/#automated-kpi-slides) or by [leveraging Sisense in Google Slides](/handbook/finance/key-meetings/#leveraging-sisense-in-google-slides).

## How to Achieve Presentations

* Engineering
* Finance
* Legal
* Marketing
* People
* Product
* Product Strategy
* Sales

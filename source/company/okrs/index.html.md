---
layout: handbook-page-toc
title: "Objectives and Key Results (OKRs)"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## Quarterly OKRs

All our OKRs are public and listed on the pages below.

- [FY21-Q2](/company/okrs/fy21-q2/)
- [FY21-Q1 (active)](/company/okrs/fy21-q1/)
- [FY20-Q4](/company/okrs/fy20-q4/)
- [FY20-Q3](/company/okrs/fy20-q3/)
- [FY20-Q2](/company/okrs/fy20-q2/)
- [FY20-Q1](/company/okrs/fy20-q1/)
- [CY18-Q4](/company/okrs/2018-q4/)
- [CY18-Q3](/company/okrs/2018-q3/)
- [CY18-Q2](/company/okrs/2018-q2/)
- [CY18-Q1](/company/okrs/2018-q1/)
- [CY17-Q4](/company/okrs/2017-q4/)
- [CY17-Q3](/company/okrs/2017-q3/)

## What are OKRs?

[OKRs](https://en.wikipedia.org/wiki/OKR) stand for Objective- Key Results and are our quarterly objectives. OKRs are _how_ to achieve the goal of the Key Performance Indicators [KPIs](/handbook/ceo/kpis/).
They lay out our plan to execute our [strategy](/company/strategy/) and help make sure our goals and how to achieve that are clearly defined and aligned throughout the organization.
The **Objectives** help us understand *what* we're aiming to do, and the **Key Results** help paint the picture of *how* we'll measure success of the objective.
You can use the phrase “We will achieve a certain OBJECTIVE as measured by the following KEY RESULTS…” to know if your OKR makes sense.
The OKR methodology was pioneered by Andy Grove at Intel and has since helped align and transform companies around the world.

OKRs have four superpowers:
* Focus
* Alignment
* Tracking
* Stretch

We do not use it to [give performance feedback](/handbook/people-group/360-feedback/) or as a [compensation review](/handbook/total-rewards/global-compensation/#annual-compensation-review).

The [Chief of Staff](/job-families/chief-executive-officer/chief-of-staff/) initiates and guides the OKR process.

Watch EVP, Engineering Eric Johnson discuss the power of OKRs from his perspective:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/aT66up3SyVU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## OKR Process

1. **All OKR Proposals must be merged by the CEO.**
1. Each executive has a maximum of 3 objectives.
1. While OKRs are known for being ambitious or committed, we only have ambitious OKRs.
1. Each objective has between 1 and 3 key results; if you have less, you list less.

Here's an overview of the OKR process using Epics and Issues.
Objectives are Epics, and KRs are in Issues.
All OKRs should cascade from the CEO Epics that are linked under the CEO's Objective.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/-KIr8lhYn7Y" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Format

<div class="panel panel-success">
**This is the format for OKRs added to the handbook page.**
{: .panel-heading}
  <div class="panel-body">
**1. Title: Objective as a sentence.**
   1. Title KR: Key result => Outcome
   1. Title KR: Key result => Outcome
   1. Title KR: Key result => Outcome
   </div>
</div>

OKRs have numbers attached to them for [ease of reference, not for ranking](/handbook/communication/#numbering-is-for-reference-not-as-a-signal)

Keep in mind the following:

1. The `=> Outcome` part is only added after the quarter started.
1. Each key result has an outcome.
1. The title is of person who is the [directly responsible individual](/handbook/people-group/directly-responsible-individuals/).
1. We use four spaces to indent instead of tabs.
1. The key result can link to an issue.
1. The outcome can link to real time data about the current state.
1. The three CEO objectives are level 3 headers to provide visual separation.
1. We number them by starting with `1.` we can more easily refer to them.

### Scoring

It's important to score OKRs after the [fiscal quarter](/handbook/finance/#fiscal-year) ends to make sure we celebrate what went well and learn from what didn't in order to set more effective goals and/or execute better next quarter.

1. Move the current OKRs on this page to an archive page _e.g._ [2017 Q3 OKRs](/company/okrs/2017-q3/)
1. Add in-line comments for each key result briefly summarizing how much was achieved _e.g._
  * "=> Done"
  * "=> 30% complete"
1. Add a section to the archived page entitled "Retrospective"
1. OKR owners should add a subsection for their role outlining...
  * GOOD
  * BAD
  * TRY
1. Promote the draft OKRs on this page to be the current OKRs

## Schedule

The EBA to the CEO is responsible for scheduling and coordination of the OKRs process detailed below.
Scheduling should be completed at least 30 days in advance of the timeline detailed below.

The number is the weeks before or after the start of the [fiscal quarter](/handbook/finance/#fiscal-year).

- -5: CEO pushes top goals to this page
- -4: E-group pushes draft updates to this page and discusses it in the e-group weekly meeting
- -3: E-group 50 minute [draft review meeting](#e-group-draft-review-meeting). All OKRs should be submitted via MR to the upcoming quarterly OKR page in advance of this meeting.
- -2: Discuss with the board and the teams
- -1: CEO reports give a [How to achieve presentation](#how-to-achieve-presentation)
- +0: CoS updates OKR page for current quarter to be active
- +2: Review previous and next quarter during the next board meeting

Emilie and Stella discuss the OKR process at GitLab in this video:
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Alhdr8BCWlw" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Levels

We only list objectives prefaced with your role title.

As a company, we do OKRs up to the team level, we don't do [individual OKRs](https://hrblog.spotify.com/2016/08/15/our-beliefs/).
Although, an individual might have OKRs if they represent a unique function.
For example, individual SDRs don't have OKRs, the SDR team does.
If Legal is one person but represents a unique function, Legal has OKRs.
Part of the individual performance review is the answer to: how much did this person contribute to the team objectives?

We have no more than [six layers in our team structure](/company/team/structure/).
In other words, this page contains OKRs for only the CEO and the [e-group](/handbook/leadership/#e-group).
This is because we leverage Epics for OKRs to [dogfood strategic planning](/company/okrs/#dogfood-strategic-planning).
By leveraging the [Health Status](https://gitlab.com/gitlab-org/gitlab/-/issues/209108) of epics, we can see progress along the way and make the issue the SSOT for the status of OKRs.

## Updating

The key results are updated continually throughout the quarter.
Every month before the [Key Meeting](/handbook/finance/key-monthly-metrics/), teams should create an MR to update their Key Results with their status, as well as present their status in the key meeting.

When presenting the status of OKRs, we use the following terms to denote the status of a key result:
1. On track - the DRI is confident the key result will be achieved.
1. Needs attention - the DRI believes there is some risk the key result will be achieved. Elevated attention is required in order for the key result to be achieved.
1. At risk - the DRI does not expect the key result will be achieved. Urgent action is required in order for the key result to be achieved.

At the conclusion of the quarter, all OKR statuses should show percentages.
Commentary should only be subsequent to percentages.
If something was punted, it should have a 0%.
If something was completed, it should have a 100%.
OKR results should be merged by the DRI's manager.

In the first month of a new quarter, key results should be updated for the previous quarter.
Everyone is welcome to a suggestion to improve any OKR.
To update please make a merge request and post a link to the MR in the #okrs channel and at-mention the CEO.
At the top of the OKRs is a link to the state of the OKRs at the start of the quarter so people can see a diff.

## Dogfood strategic planning
We should dogfood our [strategic planning features](https://gitlab.com/groups/gitlab-org/-/epics/2223) with our OKRs.
At first, we thought just a [tree view of epics](https://gitlab.com/gitlab-org/gitlab-ee/issues/7327) could make it usable, so
we started in FY21-Q1 with [the Product team using epic trees for their OKRs](https://gitlab.com/groups/gitlab-com/-/epics/262).
It was hard without being able to see [the health status of issues at a glance](https://gitlab.com/gitlab-org/gitlab/-/issues/209108).
Work happens in GitLab, and we should use GitLab to manage it.

## E-group draft review meeting

Every executive is expected to bring a draft of their OKRs to the Draft meeting that occurs three weeks prior to the start of the quarter.
In this meeting, the e-group discusses any initial concerns, discusses alignment, and highlights any possible dependencies.
OKRs will still change but this initiates the process early.

## How to achieve meeting

1. Detail how you plan to achieve your Key Results. This is not a status update on the previous quarters OKRs but a meeting to detail how you plan to achieve your proposed OKRs for the upcoming quarter.
1. CEO EBA to schedule 25 minute meeting with the E-group (CEO and functional counterpart required, everyone else optional) 1 week before the new quarter begins. EBA of CEO will also add EBA of the function leader.
1. Have your updated OKRs on the handbook page 3 weekdays in advance. Use the handbook to drive the conversation.
1. Functional Leader: Record and [upload](https://www.youtube.com/watch?v=xGwX9zjNr2E) a YouTube video with a  presentation summary to allow for additional context. This should be done at least two days before the scheduled call. If completed two days before, add the link to the recording in the `e-group` slack channel. If completed three days before the scheduled call, email the meeting participants a link.
1. EBA of the function leader to add a calendar invite for the [materials review](https://about.gitlab.com/handbook/communication/#scheduling-meetings) on the leader's calendar for three days before to remind the leader to upload the YouTube video.  
1. CEO EBA to link agenda and notes doc to the calendar invite 24 hours in advance. We'll use this to track questions and action items.
1. The meeting is fully interactive with questions being asked by other team members. No additional presentation should occur in the livestream since the presentation was already uploaded to YouTube in advance to leave time for discussion.
1. The meeting will be streamed to YouTube. A private or public stream will be indicated by the presenter to the EBA prior to their scheduled presentation.

## OKRs are stretch goals by default

OKRs should be ambitious but achievable. If you achieve less than 70% of your KR, it may have not been achievable. If you are regularly achieving 100% of your KRs, your goals may not be ambitious enough.

Some KRs will measure new approaches or processes in a quarter. When this happens, it can be difficult to determine what is ambitious and achievable because we lack experience with this kind of measurement. For these first iterations, we prefer to set goals that seem ambitious and expect a normal distribution of high, medium, and low achievement across teams with this KR.

## OKRs are what is different

The OKRs are what initiatives we are focusing on this quarter specifically.
Our most important work are things that happen every quarter.
Things that happen every quarter are measured with [Key Performance Indicators](/handbook/ceo/kpis).
Part of the OKRs will be or cause changes in KPIs.

## OKR resources:
* [With Goals, FAST beats SMART](https://sloanreview.mit.edu/article/with-goals-fast-beats-smart/)
* [Measure What Matters by John Doerr](https://www.whatmatters.com)
* [A Modern Guide to Lean OKRs](https://worldpositive.com/a-modern-guide-to-lean-okrs-part-i-c4a30dba5fa1)

## Critical acclaim

Spontaneous chat messages from team members after introducing this format:

> As the worlds biggest OKR critic, This is such a step in the right direction :heart: 10 million thumbs up

> I like it too, especially the fact that it is in one page, and that it stops at the team level.

> I like: stopping at the team level, clear reporting structure that isn't weekly, limiting KRs to 9 per team vs 3 per team and 3 per each IC.

> I've been working on a satirical blog post called "HOT NEW MANAGEMENT TREND ALERT: RJGs: Really Just Goals" and this is basically that. :wink: Most of these are currently just KPIs but I won't say that too loudly :wink: It also embodies my point from that OKR hit piece: "As team lead, it’s your job to know your team, to keep them accountable to you, and themselves, and to be accountable for your department to the greater company. Other departments shouldn’t care about how you measure internal success or work as a team, as long as the larger agreed upon KPIs are aligned and being met."

> I always felt like OKRs really force every person to limit freedom to prioritize and limit flexibility. These ones fix that!

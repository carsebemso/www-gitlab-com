---
layout: markdown_page
title: Product Direction - Fulfillment
---

## On this page
{:.no_toc}

- TOC
{:toc}

This is the product direction for GitLab's Fulfillment group. If you'd like to discuss this vision directly with the Product Manager for Fulfillment,
feel free to reach out to Mike Karampalas via [e-mail](mailto:mkarampalas@gitlab.com),
[Twitter](https://twitter.com/mkarampalas), or by [scheduling a video call](https://calendly.com/mkarampalas).

## Overview

The Fulfillment Team at GitLab focuses on creating and supporting the experiences that enable our customers to purchase, upgrade, downgrade, and renew licenses and subscriptions. We are constantly striving to make the billing and payment processes simple, intuitive, and stress-free for our customers and GitLab team members. We believe that engaging in business transactions with GitLab should be minimally invasive and when noticed, should be a positive, empowering, and user-friendly experience. 

We also partner with the [Growth Groups](https://about.gitlab.com/direction/#growth-groups) to enable experimentation and product improvements focused on making it easy for people to try, buy, grow, and stay with GitLab. 


## North Stars

* Make it easy and simple as possible to purchase, upgrade, and renew a GitLab license or subscription 
* Build and support a robust billing and licensing experience that is flexible and allows us to continue to iterate on our pricing, packaging, and product and service offerings
* Provide transparent, clear, and easy-to-understand information to admins and group owners about their users, usage, and corresponding costs 
* Automate as much of the billing process as possible so our GitLab team members can focus their time on providing excellent service and support to our customers
* Upholding our compliance, privacy, and security standards
 
## How we prioritize

We follow the same prioritization guidelines as the [product team at large](https://about.gitlab.com/handbook/product/product-management/process/#prioritization). Issues tend to flow from having no milestone, to being added to the backlog, to a directional milestone (e.g. Next 3-4 releases), and are finally assigned a specific milestone.

Our entire public backlog for Fulfillment can be viewed [on the issue board](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Afulfillment), and can be filtered by labels or milestones. If you find something you are interested in, you're encouraged to jump into the conversation and participate. At GitLab, everyone can contribute!

## What we're currently focused on 

### Making our billing more transparent

We don't want our customers to be surprised about billing and user management. Customers should be fully aware when they're taking an action that will require additional payment. 

* [Show transparent warnings when adding a user to an instance will incur a charge](https://gitlab.com/gitlab-org/gitlab/issues/35677)
* [Show transparent warnings when adding a user to a group will incur a charge](https://gitlab.com/gitlab-org/gitlab/issues/35683)
* [Initiate pro-rated charges when a .com customer exceeds the users in their subscription](https://gitlab.com/gitlab-org/customers-gitlab-com/issues/998)

### Improving user count information to admins and group owners

We want customers to easily understand what (and who) they're paying for. 

* [Indicate whether or not a user is being counted as a seat](https://gitlab.com/gitlab-org/gitlab/issues/6995)
* [Clearer Admin User Statistics page breakdown of Total and Active users](https://gitlab.com/gitlab-org/gitlab/issues/118592)
* [API call to pull user/seat data from GitLab.com](https://gitlab.com/gitlab-org/gitlab/issues/35454)


### Improving the user experience of our Customers Portal

The Customers Portal should be easy-to-use and provide a consistently delightful experience that matches our other offerings.

[The Customer Portal should use our Pajamas design system](https://gitlab.com/groups/gitlab-org/-/epics/1788) 



## Where we're headed 

We are in the early phases of moving towards a simpler approach for how self-managed instances purchase and apply a license. We recognize that the current process is less than ideal for our customers, as well as our GitLab team members. The requirement to log into the Customers Portal to purchase, renew, and access your license key can be frustrating and difficult and we want to make that process an afterthought. 

What we would like to do is create a license sync that will allow customers to regularly sync their license and user counts with GitLab to streamline purchases and renewals as well as enable us to provide more flexible billing options for our customers. 

At the same time, we understand that not all of our customers are running instances that can connect to the internet or have requirements against sharing any kind of information. In those cases, we need to ensure customers have the option to continue to purchase and apply licenses as they do today. 

We are just beginning the technical discovery phase of this effort and we hope to have an initial iteration ready by mid-2020. If you would like to discuss this, or anything else Fulfillment related, please reach out to Mike Karampalas via [e-mail](mailto:mkarampalas@gitlab.com).

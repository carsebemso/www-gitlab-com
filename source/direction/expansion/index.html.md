---
layout: markdown_page
title: Product Direction - Expansion
description: The expansion group at GitLab focuses on the upgrade and add-on workflows for existing customers
---

## Overview

The Expansion Team at GitLab focuses on running experiments to increase the expansion of our platform and expanding usage by teams within an organization or by individual users additionally we strive to increase the value of Gitlab to existing customers getting them to adopt new features in higher paid tiers. It's easy to look at GitLab and quickly come to a conclusion that our platform is for technical teams only (e.g. developers and engineers) but in fact many other non-technical teams can use GitLab to increase efficiency and integrate their work into a company's development life-cycle. To call out a few Product Managers, Data Analysts, Marketers, Support, UI/UX and Executives use GitLab in their day-to-day. We want to get these teams to that ‘ah-Ha!’ moment so they can also benefit from using the platform. Our experiments are all public and we encourage feedback from the community and internal team. 

### Expansion Team

Product Manager: [Tim Hey](/company/team/#timhey) | 
Engineering Manager: [Phil Calder](/company/team/#pcalder) | 
UX Manager: [Jacki Bauer](/company/team/#jackib) | 
Product Designer: [Matej Latin](/company/team/#matejlatin) | 
Full Stack Engineer: [Doug Stull](/company/team/#dougstull) | 
Full Stack Engineer: [Jackie Fraser](/company/team/#jackie_fraser)

### Expansion Team Mission

*   Expand ARR for current customers by driving seat expansion, upgrades, add ons, reduced discounts, etc.

### Expansion KPI

*   [Net retention](https://about.gitlab.com/handbook/customer-success/vision/#financial-kpis)

_Supporting performance indicators:_

*   (#) of seats added 
*   % of seats added
*   Seats by stage 
*   Seats added by tier
*   Session level Usage (ping data)
*   (#) Seats by territory (where in the world)
*   (#) Seats by buyer Persona type ([buyer](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#buyer-personas) persona)
*   User Persona type ([user](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#user-personas) persona)
*   Source of user acquisition (use channel e.g. paid, earned, social, owned)
*   Account usage
*   Seat utilization by account (how many seats did they sign up for and how many are they using)
*   % of instances (and groups, for GitLab.com) using more than ‘x’ stages 
*   MAU - For all stages - The number of unique users who had at least one session within a 28-day period. The 28-day period includes the last day in the active date range.
*   SMAU - Monthly active users by stage - this will be a subset of MAU at the stage level
*   SMAU Factor - expressed as the average number of stages used by users
*   Feature MAU - Monthly active users by feature - this will be a subset of SMAU
*   Feature MAU Factor - expressed as the average number of features by SMAU
*   Usage Pings - How many ping counts were created by all users
*   User Count Free - How many users were in the 'Free' plan
*   User Count Bronze - How many users were in the 'Bronze' plan
*   User Count Silver - How many users were in the 'Silver' plan
*   User Count Gold - How many users were in the 'Gold' plan
*   Total User Count - How many total users who have an account with GitLab

### Our approach

Expansion runs a standard growth process. We gather feedback, analyze the information, ideate then create experiments and test. As you can imagine the amount of ideas we can come up with is enormous so we use the [ICE framework](https://blog.growthhackers.com/the-practical-advantage-of-the-ice-score-as-a-test-prioritization-framework-cdd5f0808d64) (impact, confidence, effort) to ensure we are focusing on the experiments that will provide the most value. 

### Maturity

The growth team at GitLab is new and we are iterating along the way. As of August 2019 we have just formed the expansion group and are planning to become a more mature, organized and well oiled machine by January 2020. 

### Problems we solve

Do you have issues... We’d love to hear about them, how can we help GitLab contributors find that ah-Ha! Moment. 

Here are few problems we are trying to solve: 

*   **User Orientation** - Users don’t know where to start
    *   As a product manager, where do I start?
    *   What are the best tools for me to use on this platform as a security engineer? 
*   **Increase platform confidence and trust** - I love my tools and am afraid to switch
    *   We know our users love the tools they are currently using and that change is hard. We are trying to earn your trust and show you how to use our platform as one solution.
*   **Visibility** - Ever get that email, you know the one “can you give me an update on…” when individual contributors, management and executives are all on the same platform visibility increases 10 fold. 
    *   The more teams from your company using GitLab will dramatically improve visibility by reducing friction and noise and help you stay focused. 

### What user workflows does the expansion team focus on?

The user workflows on GitLab.com and our Self-managed instances are very different and should be treated separately. We will be focusing on the 2 described below. *note: our sales team outlines the process in [point 3. on this page](https://about.gitlab.com/handbook/business-ops/resources/#opportunity-types). (Again the sales motion is very different than the self serve experience on GitLab.com).*

*  **Add-on purchases** - ci minutes ([dashboard](https://app.periscopedata.com/app/gitlab/434763/CI-Minutes)), seats etc. Note: seat adds for expansion happen outside the renewal process think of opportunities to add more users through projects and groups vs. adding users during renewal. 

*  **Tier upgrades** - The movement of customers from tier to tier can be found in this [dashboard](https://app.periscopedata.com/app/gitlab/484507/Churn-%7C-Expansion-by-Sales-Segment). Keep in mind we're focused on customers who are already paying subscribers no free to paid. Here is the [UX Scorecard for upgrading accounts](https://gitlab.com/gitlab-org/growth/product/issues/113)

### Key User Focus:

#### External
The external user personas on GitLab.com and our Self-Managed instances are very different and should be treated as such. We will be focusing on the 2 described below. 

In addition to the personas, it's also important to understand the permissions available for users (limits and abilities) at each level. 
* [Handbook page on permissions in GitLab](https://about.gitlab.com/handbook/product/#permissions-in-gitlab)
* [Docs on permission on docs.gitlab.com](https://docs.gitlab.com/ee/user/permissions.html)
* *Note: GitLab.com and Self-Managed permission do differ.*  

#### GitLab Team Members
*  Sales Representatives
*  Customer Success Representatives
*  Support Representatives

### What's keeping us busy

|  **Hypothesis** | **Issue** | **Impact** | **Confidence** | **Effort** | **ICE Score** | **Status** |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  If we create a modal that can be used in various places in the the app. We can make it easier for users to invite their team members through their workflow. | [Move the "invite members" form into a modal and create a component that can then be consistently used throughout the app](https://gitlab.com/gitlab-org/gitlab/-/issues/214425) | 8 | 8 | 4 | 6.7 | in design |
|  If we promote our users to invite their team members to a group we will increase the adoption of groups. | [Increase group adoption of GitLab by increasing the motivation and improving triggers for inviting team members](https://gitlab.com/gitlab-org/gitlab/-/issues/213646) | 9 | 8 | 8 | 8.3 | Validation Backlog |
|  If we prompt users to add team members to a group we will increase the total # of users per group | [Prompt a user to add additional members to a single member group](https://gitlab.com/gitlab-org/gitlab/-/issues/8063) | 9 | 8 | 4 | 7.0 | Problem Validation |
|  If we shift the way users are added to groups only we can reduce the # of nested users and increase the # of users in groups. | [Automatically bill for newly added members in personal namespaces](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/465) | 5 | 7 | 9 | 7.0 | Problem Validation |
|  If we prompt paid users to add pipelines when they have already authored an MR we can increase pipelines which leads to ci minute usage and a potential add on sale. | [MR with no pipelines, suggest pipeline](https://gitlab.com/groups/gitlab-org/growth/-/epics/14) | 7 | 9 | 4 | 6.7 | Live Experiment |
|  If we increase the places we display an option to buy minutes when a customer is running low we will increase the # of ci minute purchases. | [Add a 'buy ci minutes' option in the top right drop down (user settings menu) on GitLab.com](https://gitlab.com/groups/gitlab-org/growth/-/epics/28) | 3 | 7 | 8 | 6.0 | Staging |
|  If we display a notification dot over the user's avatar when they are running out of ci minutes they will expand the user settings menu and be shown a buy ci minutes button and this will lead to more ci minute add on purchases. | [Add a notification dot when 'buy ci minutes' button is displayed in the user settings on GitLab.com](https://gitlab.com/groups/gitlab-org/growth/-/epics/30) | 3 | 8 | 8 | 6.3 | Staging |
|  If we improve the ci minutes checkout process we will increase ci minute purchases and reduce support tickets. |[ Make the "Buy add-on CI minutes" process simpler and clearer](https://gitlab.com/groups/gitlab-org/growth/-/epics/33) | 5 | 8 | 9 | 7.3 | Solution Validation |
|  If we add an upgrade button to the user settings menu on gitlab.com we will see more upgrades. | [Add an 'upgrade' option in the top right drop down (user settings menu) on GitLab.com](https://gitlab.com/gitlab-org/growth/product/-/issues/843) | 3 | 8 | 8 | 6.3 | In Dev |
|  If we add a link to the docs page on how to upgrade your account for self-managed instances we will increase self-serv upgrades. | [Add a link to "account upgrade documentation" for self-managed users in the UI](https://gitlab.com/gitlab-org/growth/product/-/issues/286) | 4 | 7 | 7 | 6.0 | Blocked |
|  If we create an new account overview page for self-managed instance with all the information a customer would need to make an educated decision around upgrading we would increase more self-serv upgrades. | WIP: [Create an "account overview" page for self-managed instances](https://gitlab.com/gitlab-org/growth/product/-/issues/289) | 6 | 6 | 8 | 6.7 | Problem Validation |
|  If we gave a user and estimate for an upgrade we would increase upgrades. | [Give the user an estimate for the upgrade](https://gitlab.com/gitlab-org/growth/product/-/issues/291) | 5 | 5 | 3 | 4.3 | Validation Backlog |
|  If we introduced an easier way to contact sales about upgrading a self-managed account we would reduce customer stress and increase sales serv opportunities for upgrades. | Introduce an easier way to contact sales about upgrading a self-hosted instance plan | 7 | 5 | 3 | 5.0 | Validation Backlog |
|  If we enabled the ability to update an existing license to a new tier we would increase upgrades and reduce licenses app access. | [Add the possibility to update a license to a different plan](https://gitlab.com/gitlab-org/growth/product/-/issues/287) | 7 | 4 | 9 | 6.7 | Validation Backlog |
|  If we update the documentation on how to upgrade your self-managed plan we will improve our customer experience and increase upgrades |[ Update the existing documentation for upgrading self-managed subscriptions](https://gitlab.com/gitlab-org/growth/product/-/issues/288) | 2 | 9 | 3 | 4.7 | In Production |

### Apps & Services we focus on:
*   [GitLab Enterprise Edition (Self-Managed)](https://about.gitlab.com/handbook/engineering/projects/#gitlab)
*   [GitLab-com (SaaS)](https://gitlab.com/gitlab-com/www-gitlab-com#www-gitlab-com)
*   [Version.GitLab](https://about.gitlab.com/handbook/engineering/projects/#version-gitlab-com)
*   [Customers.GitLab](https://about.gitlab.com/handbook/engineering/projects/#customers-app)
*   [License.GitLab](https://about.gitlab.com/handbook/engineering/projects/#license-app)

### Helpful Links
*   [Expansion Team Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion)
*   [Expansion Epic](https://gitlab.com/groups/gitlab-org/-/epics/2358)
*   [Growth Section](https://about.gitlab.com/handbook/engineering/development/growth/)
*   [UX Scorecards for Growth](https://gitlab.com/groups/gitlab-org/-/epics/2015)
*   [GitLab Growth project](https://gitlab.com/gitlab-org/growth)
*   [KPIs & Performance Indicators](https://about.gitlab.com/handbook/product/metrics/)

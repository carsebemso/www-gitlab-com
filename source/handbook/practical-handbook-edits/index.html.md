---
layout: handbook-page-toc
title: "Practical Handbook Edits Examples"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the Practical Handbook Edits Examples Page 
This page will contain video recordings of the "Handbook-First Field Enablement" team meetings held by the [Field Enablement](/handbook/sales/field-operations/field-enablement/) team. In these meetings, the team runs through the GitLab Handbook with experts, uncovering how to best use the Handbook in our day-to-day work, and learning best-practices for Handbook editing along the way. 

Have your own practical Handbook editing tips? Drop a video below!

### Creating new handbook pages and multimedia embedding best-practices
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/hQgS97M8abc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

This video covers:
- Creating a new handbook page - @:37
- Embedding a video - @15:25, @18:53
- Making a URL open in a new tab - @17:05
- How this page got started - @22:48

### Changing a page name and subsequent updates
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/HeQax_U74NM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

This video covers:
- Renaming a URL - @1:05
- Redirecting from one URL to the other - @2:17
- Finding places where an old URL is linked and updating it to a new URL - @ 4:30

### Creating Mermaid Diagrams
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/SQ9QmuTHuSI" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

This video covers:
- Creating a mermaid diagram for the handbook: 
   - Intro to a mermaid diagram  
   - What they look like
   - Use cases for using them in the handbook

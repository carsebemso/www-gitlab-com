---
layout: handbook-page-toc
title: "Recruiting Process - Candidate Experience Specialist Tasks"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Recruiting Process Framework - Candidate Experience Specialist Tasks
{: #framework-coord}

### Step 11/C: Schedule first interview

Once a candidate has provided their availability in Greenhouse, the Candidate Experience Specialist will utilize the internal [hiring processes repository](https://gitlab.com/gitlab-com/people-ops/hiring-processes) to determine scheduling needs. If the repo is outdated or you are unsure of the interview process, reach out to the recruiter.

### Step 14/C: Schedule team interviews

Once a candidate has provided their availability in Greenhouse for the next step in the interview process, the Candidate Experience Specialist will utilize the internal [hiring processes repository](https://gitlab.com/gitlab-com/people-ops/hiring-processes) and schedule all interviews in accordance with the interview plan. The candidate is moved to the next step in the interview plan by a yes or strong yes scorecard. The Candidate Experience Specialist should be sure to inform the candidate that each subsequent interview is contingent on the success of the prior interview.

Any candidates who receive a no or definitely not scorecard should be discussed with the Recruiter and the Hiring Manager before declining.

#### Executive Interview Scheduling

For Directors and above with Executive Business Admins:

   1. Prior to candidates moving to the Executive interview stage the Candidate Experience Specialist (CES) ensures the candidate's profile contains a resume/cv,  contact phone number and feedback from prior interviews.
   1. The CES then moves the candidate to Executive Stage in Greenhouse (if applicable).
   1. The CES requests availability from the candidate.
   1. Once the availability is back from the candidate the CES will ping [the appropriate Executive Business Admin (EBA)](/handbook/eba/#executive-business-administrator-team) in the Greenhouse notes section and will include any relevant details such as internal, timezone or high priority.
   1. The EBA will schedule the interview using a unique zoom link, generated by the EBA, and will send out the interview confirmation email to the candidate with a cc to the candidate's CES.
       * For the EVPE, all interviews should be scheduled for 60 minutes, the EBA will not use "speedy meetings" for EVPE's interviews.
   1. If the EBA is unable to find a suitable time within the candidate's given availability the EBA should suggest times that will work, see the next section for details.

For the VP of Alliances, VP of Commercial Sales, VP of Customer Success, VP of Enterprise Sales, VP of Field Operations and the VP of Worldwide Channels:

   1. Prior to candidates moving to the Executive interview stage the Candidate Experience Specialist (CES) ensures the candidate's profile contains a resume/cv,  contact phone number and feedback from prior interviews.
   1. The CES then moves the candidate to Executive Stage in Greenhouse (if applicable).
   1. The CES pings [the appropriate Executive Business Admin (EBA)](/handbook/eba/#executive-business-administrator-team) in the Greenhouse notes section requesting interview options and will include any relevant details such as internal, timezone or high priority.
   1. The EBA will @ mention the CES in the Greenhouse note section with approximately 3 interview options and specify the timezone the options are given in.
   1. The CES sends the candidate the suggested times using the Candidate Availability for Executive Interviews email template.
   1. After the Candidate responds indicating the time that works for them the CES schedules the interview and uses the interviewer's personal zoom link.
   1. The CES sends the interview confirmation email with a cc to the EBA.

If you have any questions about the process, please post in #eba-team slack channel and @ mention the EBA to the CEO.

#### Interview Reschedule Requests and Other Communication to the CES Email

The CES team utilizes [GitLab Service Desk](/product/service-desk/) to track incoming emails to the CES email alias.

1. Under this [CES Service Desk Project](https://gitlab.com/gl-recruiting/ces-service-desk) set up the proper notifications
   - Click on the bell icon on the top right next to Star and Clone
   - Go to Custom Settings
   - Check "New issue"
   - Closeout the window
1. On the left-side menu bar click Issues
   - This is where all our incoming CES emails will create an issue. You'll get an alert when someone sends an email to the CES email alias. Any "emails" that need to be addressed will be an open issue listed within this project.
1. Click on the new Issue
1. Assign it to yourself on the right-side toolbar
1. Read the Issue message
1. Add the appropriate label
1. Respond to the "email" by adding comments to the issue (be sure to enter comments as you would an email to the candidate)
   - The response might be as simple as Dear x, thank you for giving the team a heads up on the change of your schedule. I've canceled your interview and we will reach out to reschedule after taking a look at schedules.
1. If this one comment address the entire message and the Application Tracking System (ATS) is the tool needed, add a comment and close the issue
   - If the one comment does not address the entire message then only select comment.
1. Navigate to the ATS, if you are unable to reschedule or reset-up yourself tag the appropriate CES in the applicable greenhouse profile notes and copy the issue link in the comment (i.e. @Jane please reschedule with John per https:xxxxx)

There may be situations where the CES email is used to thank the team or to send a follow-up note. In those cases, we would copy the text of the issue to forward to the appropriate parties through a greenhouse profile at mention.

### Step 20/C: Initiate background check

Once notified by the recruiter, the Candidate Experience Specialist will [initiate a background check](/handbook/people-group/code-of-conduct/#background-checks) for the candidate. The Candidate Experience Specialist will continue to monitor the background check until finalized, utlizing the follow-up feature in Greenhouse to ensure the background check is complete and uploaded into BambooHR if hired.

For guidance on reviewing Background checks see [this page](/handbook/people-group/code-of-conduct/#background-checks).

#### Initiating a Background Check through Greenhouse

**US Candidates Only**

1. Log in to [Greenhouse](https://app2.greenhouse.io/dashboard) and go to the candidate's profile.
1. Click the "Private" tab.
1. Click "Export to TalentWise".
1. Click "Complete Report", which will redirect you to the Sterling website.
1. Scroll down and click "Add Screening".
1. Next to "Comprehensive Criminal with Employment", click on "Ticket". If you need to run a financial check as well for Finance team members, after you click "Ticket", click "Add Products" on the right and search for and include "Federal Criminal District Search".
1. Check off that you agree to your obligations as a user.
1. Under "Disclosure and Authorization Options", select the first option to have Sterling send the candidate a disclosure form.
1. Click "Generate Ticket".

#### Initiating a Background Check through Sterling Talent Solutions

**US Candidates Only**

1. Log in to [Sterling](https://www.talentwise.com/screening/login.php) and select "Quick Launch".
1. Click "Launch Screening".
1. Next to "Comprehensive Criminal" click on "Ticket". If you need to run a credit check as well, after you click "Ticket" click "Add Products" on the right and search for "Federal Criminal Check".
1. Check off that you agree to your obligations as a user.
1. Enter the candidate's name and personal email address.
1. Select the first option to have Sterling send the candidate a disclosure form, and click "Generate Ticket".

**Non-US Candidates Only**

1. Log in to [Sterling](https://secure.sterlingdirect.com/login/Default.aspx) and E-invite the candidate by inputting their email address.
1. Under "Applicant Information" enter in the candidate's first and last name, as well as their email address to confirm.
1. Next, select "International" from the "Job Position" drop down menu.
1. Next, select "A La Carte" from the "Screening Packing".
1. Then, you will select "Verification- Employment (International)" from the "A La Carte" drop down. Then click "Add".
1. After that, you will select "Criminal-International". A drop down menu will appear, and you will select the country the candidate is located in. Then click "Add"
1. If you are submitting a background check for a candidate located in Japan, you will select "GlobeX" instead of "Criminal-International". Then select "Japan" and click "Add"
1. Make sure both checks are included in the "Search" box.
1. Finally, scroll to the bottom of the page and click "Send"

### Step 23/C: Send contract

[See Contracts section of the handbook](/handbook/contracts/#how-to-use)

One person from the recruiting team (typically the [Candidate Experience Specialists](/job-families/people-ops/candidate-experience/)) will prepare the contract. While the Candidate Experience Specialist will prioritize a contract above other tasks, the expected turn around on the task is 1 business day. If the contract is time-sensitive, ping the CES team on the candidate's Greenhouse profile and provide context for the rush. If the Candidate Experience Specialist cannot meet the 1 business day they will inform the recruiter via Greenhouse and will provide context.

   1. Check all aspects of the offer:
      - Do we have the new team members' legal name in their profile?
      - Is the new team members' address listed on the details page?
      - What contract type and entity are required based upon location and offer details?
      - Is it clear how many (if any) stock options this person should receive?
      - Is all necessary information (start date, salary, location, etc.) up to date?
      - Does the new team member need a work permit or visa, or require an update to them before a start date can be agreed?
      - Has the signatory been determined by the Candidate Experience Specialist and updated?
      - Has the Entity been selected based on the New Hire's location?
   1. [Generate the contract within Greenhouse](/handbook/contracts/#how-to-use) using a template based on the details found in the offer package.
   1. Contact the recruiter or new team member to gather any missing pieces of information (note: the address can be found on the background check information page).
   2. The Signatory will be either the Recruiting Manager, VP of Recruiting, or the Chief People Officer. This can be determinted by the Candidate Expereince Specialist sending a message to the Contracts to Sign channel in Slack.
   3. The entity will be selected based on the new hire's location.
   1. Ensure that, if the contract was created outside of Greenhouse, the contract has been reviewed and approved by the Senior Director of Legal Affairs or a Total Rewards Analyst.
   1. [Stage the contract in DocuSign from within Greenhouse](/handbook/contracts/#how-to-use), which emails the contract to the signing parties, with the recruiter, recruiting manager, and the hiring manager cc'd. It will be sent to the designated signatory as previously determined in Offer Details.
   1. Enter the new team member's details on the Google sheet [GitLab Onboarding Tracker](https://docs.google.com/spreadsheets/d/1L1VFODUpfU249E6OWc7Bumg8ko3NXUDDeCPeNxpE6iE/edit?usp=sharing) and continually update it during the offer process.  If the new team member is based outside the US, add their country of residence to the I-9 column on the tracker.  
   1. When the contract is signed by all parties, the Candidate Experience Specialist will verify that the start date in Greenhouse is correct. They will then mark the candidate as "Hired" in Greenhouse. Please note, the new hire's BambooHR profile will be generated automatically on an hourly basis.
   1. Once the new hire's profile in BambooHR is generated, The Candidate Experience Specialist will upload the signed contract and the completed background check into the BambooHR profile.
   1. The Candidate Experience Specialist will send an email to total-rewards@domain with any variations in contract language (for example a draw). Compensation will sync with Payroll and Sales Ops for any necessary notifications on payment types.
   1. The Candidate Experience Specialist will email the new team member the Welcome Email from Greenhouse with a Cc to the recruiter, IT Ops and hiring manager.  For new team members in USA, use 'GitLab Welcome - US only' template.  For team members located outside the US, use 'GitLab Welcome - non US' template
      * Instructions on the [Notebook Ordering Process](/handbook/business-ops/it-ops-team/#laptops) are included with this email.
   1. The recruiter will unpublish the vacancy in Greenhouse for the internal job board and disposition any remaining candidates if necessary. Once complete, the recruiter will ping the Candidate Experience Specialist to close the role or close the role themselves.
   1. The recruiter should create a MR on the [Careers page](/jobs/careers/) to remove the vacancy from the list. The CES should audit that this step was complete.
   1. The final step before handing off to the People Operations Specialists is for the Candidate Experience Specialist to complete the  on the "CES Steps Complete" column on the [GitLab Onboarding Tracker](https://docs.google.com/spreadsheets/d/1L1VFODUpfU249E6OWc7Bumg8ko3NXUDDeCPeNxpE6iE/edit?usp=sharing). Should the start date change after the welcome email is sent please see the required steps [here](/handbook/contracts/#how-to-update-a-start-date-after-the-contract-is-signed).
   1. Exception to the start date and onboarding date alignment: If a new team member requires a specific start date for legal reasons (cannot have break in employment) but the People Operations Team cannot start onboarding on that specific day (because of Public Holiday), the Candidate Experience Specialist adds a note about the exception to the Important notes from CES column of the [GitLab Onboarding Tracker](https://docs.google.com/spreadsheets/d/1L1VFODUpfU249E6OWc7Bumg8ko3NXUDDeCPeNxpE6iE/edit?usp=sharing). The Contract, Greenhouse and BambooHR should reflect the same start date regardless of the actual onboarding date. However the GitLab Onboarding Tracker should reflect the actual onboarding date. 

The Candidate Experience Specialist will make it a part of their weekly tasks to check the [GitLab Onboarding Tracker](https://docs.google.com/spreadsheets/d/1L1VFODUpfU249E6OWc7Bumg8ko3NXUDDeCPeNxpE6iE/edit?usp=sharing) to ensure their new team members are handed off properly to the People Operations Specialist.

### Mid-Point Check-In Follow-up

The Candidate Experience Specialist will set up a delay send email using the "GitLab Helpful Links - Checking In" email template in GreenHouse. The delay should be set up for halfway between the contract being signed and the start date.
   1. Navigate to the Candidate's profile in GreenHouse.
   1. Under Tools Email Candidate
   1. From the Template drop-down select: GitLab Helpful Links - Checking In
   1. From the Send email when drop-down select: Pick a custom time...
   1. From the calendar select a future date and time, halfway between now and the Candidate's start date.
   1. Check Schedule Email.

If necessary you may cancel the email, which is now showing under the Email Candidate option.

### Next Steps

People Operations Specialist will create the onboarding issue and start the [onboarding tasks](/handbook/general-onboarding/onboarding-processes/) no later than one week before the new team member joins. Should a contract not be signed prior to 4 working days from the start date, a new start date will be required.

For questions about the new team member's onboarding status, view the People Operations Specialists assigned via the Google sheet [GitLab Onboarding Tracker](https://docs.google.com/spreadsheets/d/1L1VFODUpfU249E6OWc7Bumg8ko3NXUDDeCPeNxpE6iE/edit?usp=sharing) and @mention them in the `#peopleops-confidential` Slack channel.

For questions about the new team member's laptop, ping [IT Ops](#it-ops) in Slack. If the questions arise through email, forward the email to itops@gitlab.com and ping IT Ops in #it-ops Slack, and @it-ops-team too due to volume.

### Interview Reimbursement Process

For candidates requesting [interview reimbursment](https://about.gitlab.com/handbook/hiring/interviewing/#reimbursement-for-interviewing-with-gitlab) the CES team will partner with the Account Payable (AP) team to ensure requests are processed confidentially and in a timely manner. AP and the CES team utilize [GitLab Service Desk](/product/service-desk/) to track incoming emails to the Interviews@gitlab.com email.

Under the [Interview Reimbursement Service Desk](https://gitlab.com/interview-reimbursement/ap-ces/issues) set up the proper notifications
   - Click on the bell icon on the top right next to Star and Clone
   - Go to Custom Settings
   - Check "New issue"
   - Closeout the window

Additional process details can be found on the [project README page](https://gitlab.com/interview-reimbursement/ap-ces/blob/master/README.md).

---
layout: handbook-page-toc
title: "Open Source Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab for Open Source Program Overview
*At GitLab, our mission is to change all creative work from read-only to read-write so that everyone can contribute.* The GitLab for Open Source program exists to support communities that are also pursuing this goal by offering them our top tiers for free. 

Qualifying Open Source projects can start benefiting from self-hosted Ultimate and cloud-hosted Gold by completing a simple [application process](https://about.gitlab.com/solutions/open-source/program/).

### What's included?
- GitLab's top tiers (self-hosted Ultimate and cloud-hosted Gold) are [free for OSS projects](/solutions/open-source/)

### Who qualifies for the GitLab for Open Source program?

- Any project that uses an OSI-approved open source license and which does not seek to make a profit from the resulting project software may apply.
- See our [full terms](/terms/#edu-oss) regarding this program
- For more questions, please see the [FAQ section](/solutions/open-source/#FAQ)

## Deep Dive into the GitLab for Open Source program
If you're interested in learning more about how the GitLab for Open Source program operates, or if you are working on it directly and are looking for more information, please read on. 

## Where to find what we are working on
### Goals for current quarter
GitLab creates [Objectives and Key Results (OKRs)](https://en.wikipedia.org/wiki/OKR) per quarter. Here are the current OKRs for the GitLab for Open Source program:
 * [Revamp OSS Program to support growth](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics/10)
 * [Build up OSS program relationships](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics/11)

### Epics
 * [Open Source Program Epics](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Open%20Source)

### Issue boards to track work
These are 
 * [Public migrations](https://gitlab.com/gitlab-org/gitlab/-/boards/1625117?label_name[]=Open%20Source&label_name[]=movingtogitlab) - This issue board tracks current migrations and tracks any blocker issues, or product feature needs that they may have
 * [Private migrations](https://gitlab.com/groups/gitlab-com/-/boards/1622882?&label_name[]=Open%20Source%20Program&label_name[]=movingtogitlab) - This is full of confidential issues visible only to GitLab team members. It tracks migrations that have not been made public as well as allows the GitLab team to coordinate on any private issues relating to migration projects. 
 * [Open Source Program project board](https://gitlab.com/groups/gitlab-com/-/boards/1622883?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=movingtogitlab&label_name[]=Open%20Source%20Program) - This tracks the internal work happening to support our GitLab for Open Source Program. 
 * [Features related to GitLab for Open Source program](https://gitlab.com/gitlab-org/gitlab/-/boards/1625116?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Open%20Source&not[label_name][]=movingtogitlab) - This is a list of features or product updates that relate to the GitLab for Open Source program. 

### Labels
 * ["Open Source Program"](https://gitlab.com/groups/gitlab-com/-/issues?label_name%5B%5D=Open+Source+Program) - this label is part of the gitlab/gitlab-com group and is applied to any issues related to the GitLab for Open Source program.
 * ["Open Source"](https://gitlab.com/gitlab-org/gitlab/-/issues?label_name%5B%5D=Open+Source) - this label is part of the gitlab/gitlab-org group and is applied to issues that propose features that would be useful for open source projects and maintainers. It is meant to track product-related work.
 * "movingtogitlab"- this label exists in both the [gitlab-org group](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=movingtogitlab) and the [gitlab-com group](https://gitlab.com/groups/gitlab-com/-/issues?label_name%5B%5D=movingtogitlab). It helps track migrations. 
 * Scoped labels
   * Migration or product-related: OSS Stage | Blocked, Evaluation, In Progress, Review 
   * Work-related: mktg-status | triage, plan, blocked, ready-to-begin, wip, review

## Other resources

- Community Advocacy [workflow](/handbook/marketing/community-relations/community-advocacy/workflows/education-oss-startup/)
- GitLab OSS program [repository](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/gitlab-oss/)

## Metrics

SFDC Opportunities by Stage for OSS campaign - SFDC [report](https://na34.salesforce.com/00O61000004hfom)
